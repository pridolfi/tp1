/*
 * sensores.c
 *
 *  Created on: Apr 10, 2021
 *      Author: pablo
 */

#include "main.h"


void sensores_loop(void)
{
	  if (HAL_GPIO_ReadPin(SENSOR1_GPIO_Port, SENSOR1_Pin)) {
		  // do stuff
		  // ejemplo - activar una salida
		  HAL_GPIO_WritePin(ALARMA_GPIO_Port, ALARMA_Pin, SET);

	  } else {
		  HAL_GPIO_WritePin(ALARMA_GPIO_Port, ALARMA_Pin, RESET);

	  }
}
